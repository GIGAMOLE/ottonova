# Ottonova

All the application structure has done from scratch

This project was written by using modern way of Android Development. I used MVVM/Clean Architecture, Koil, Koin, BRVAH, Room. This application has a dynamic network listener and offline mode.

Have a nice day :)

## Preview

Base:  
![](/gifs/base.gif)

Offline:  
![](/gifs/offline.gif)

## Author

[Basil Miller](https://www.linkedin.com/in/gigamole/)  
[+380 50 569 8419](tel:380505698419)  
[gigamole53@gmail.com](mailto:gigamole53@gmail.com)
