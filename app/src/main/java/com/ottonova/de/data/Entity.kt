package com.ottonova.de.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Profile(

    /*{
        "profile_id": "freemium_profile",
        "first_name": "Otto",
        "last_name": "Nova",
        "gender": "male",
        "display_name": "Otto Nova",
        "is_primary_profile": true,
        "policy_number": "000000001",
        "tariff_id": "chi-first",
        "tariff_label": "First Class",
        "tariff": {
        "id": "chi-first",
        "label": "First Class",
        "icon": {
            "url": "https://freemium.ottonova.de/api/images/tariffs/tariff_freemium_first_class.png",
            "primary_color": "#667093",
            "secondary_color": "#999FB7"
        },
        "excess_rate": 10
    },
        "deductible": {
        "total": 50000,
        "remaining": 27050
    },
        "date_of_birth": "1989-01-01",
        "address": {
        "street": "Ottostr.",
        "street_number": "4",
        "zip": "80333",
        "city": "MÃ¼nchen"
    },
        "contact": {
        "email": "feedback@ottonova.app",
        "phone": "089 12 14 07 12"
    },
        "profile_attributes": [
        "freemium",
        "signup_complete",
        "appointment_booking_available",
        "health_x_complete"
        ]
    }*/

    @SerializedName("profile_id")
    @PrimaryKey val id: String,
    @SerializedName("display_name")
    val displayName: String
)

@Entity
data class HealthPrompt(

    /*{
        "uuid": "freemium_impuls_201902",
        "message": "Gesundheitsimpuls 02/19\nStell dir heute mal den Wecker zum Schlafengehen.",
        "permanent": true,
        "display_category": "ottonova",
        "metadata": {
            "link": {
                "title": "Mehr zu gesundem Schlaf",
                "url": "https://www.ottonova.de/health/drum-schlaf-auch-du"
           }
        }
    }*/

    @SerializedName("uuid")
    @PrimaryKey val id: String,
    @Embedded(prefix = "metadata")
    val metadata: Metadata
)

data class Metadata(
    @Embedded(prefix = "link")
    val link: Link
)

data class Link(
    val title: String,
    val url: String
)

@Entity
data class TimelineEvent(

    /*{
        "uuid": "freemium_timeline_event_2",
        "timestamp": "2020-03-15T08:00:00+00:00",
        "display_category": "dentist",
        "title": "Zahnarztbesuch",
        "description": "Dr. Medicus Dent",
        "category": "doctor_visit"
    }*/

    @SerializedName("uuid")
    @PrimaryKey val id: String,
    val timestamp: String,
    val title: String,
    val description: String
)
