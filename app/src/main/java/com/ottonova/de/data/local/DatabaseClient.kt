package com.ottonova.de.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ottonova.de.data.HealthPrompt
import com.ottonova.de.data.Profile
import com.ottonova.de.data.TimelineEvent
import org.koin.dsl.module

object DatabaseClient {

    private const val DATABASE_NAME = "ottonova_database"

    // DB module
    val module = module {
        single { provideOttonovaDatabase(get()) }
        single { get<OttonovaDatabase>().profileDao() }
        single { get<OttonovaDatabase>().healthPromptDao() }
        single { get<OttonovaDatabase>().timelineEventsDao() }
    }

    private fun provideOttonovaDatabase(context: Context) = Room.databaseBuilder(
        context.applicationContext,
        OttonovaDatabase::class.java,
        DATABASE_NAME
    ).build()

    @Database(
        entities = [
            Profile::class,
            HealthPrompt::class,
            TimelineEvent::class
        ],
        version = 1,
        exportSchema = false
    )
    abstract class OttonovaDatabase : RoomDatabase() {
        abstract fun profileDao(): ProfileDao
        abstract fun healthPromptDao(): HealthPromptDao
        abstract fun timelineEventsDao(): TimelineEventDao
    }
}
