package com.ottonova.de.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ottonova.de.data.HealthPrompt

@Dao
interface HealthPromptDao {

    // Right now we will be not using profileId to select them from the DB
    @Query("SELECT * from healthprompt")
    suspend fun getAll(): List<HealthPrompt>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    suspend fun insertAll(healthPrompts: List<HealthPrompt>)
}
