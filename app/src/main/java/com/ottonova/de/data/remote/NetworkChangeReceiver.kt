package com.ottonova.de.data.remote

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class NetworkChangeReceiver(
    private val networkChecker: ApiClient.NetworkChecker
) : BroadcastReceiver() {

    lateinit var onNetworkChanged: (Boolean) -> Unit

    // We are not using any intent-action check, so let's suppress it
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context?, intent: Intent?) {
        onNetworkChanged(
            networkChecker.isAvailable()
        )
    }
}
