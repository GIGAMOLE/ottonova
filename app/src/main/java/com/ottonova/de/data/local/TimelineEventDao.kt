package com.ottonova.de.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ottonova.de.data.TimelineEvent

@Dao
interface TimelineEventDao {

    // Right now we will be not using profileId to select them from the DB
    @Query("SELECT * from timelineevent")
    suspend fun getAll(): List<TimelineEvent>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    suspend fun insertAll(timelineEvents: List<TimelineEvent>)
}
