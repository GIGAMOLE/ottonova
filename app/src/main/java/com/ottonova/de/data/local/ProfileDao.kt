package com.ottonova.de.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ottonova.de.data.Profile

@Dao
interface ProfileDao {

    @Query("SELECT * from profile")
    suspend fun getAll(): List<Profile>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    suspend fun insertAll(profiles: List<Profile>)
}
