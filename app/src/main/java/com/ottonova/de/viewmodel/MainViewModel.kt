package com.ottonova.de.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ottonova.de.data.Profile
import com.ottonova.de.model.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainViewModel(
    private val profilesRepository: ProfilesRepository,
    private val healthPromptsRepository: HealthPromptsRepository,
    private val timelineEventsRepository: TimelineEventsRepository,
    private val profileMapper: ProfileMapper,
    private val healthPromptMapper: HealthPromptMapper,
    private val timelineEventMapper: TimelineEventMapper
) : ViewModel() {

    private val _profileHello = MutableLiveData<String>()
    val profileHello: LiveData<String> = _profileHello

    private val _healthPrompts = MutableLiveData<List<HealthPromptModel>>()
    val healthPrompts: LiveData<List<HealthPromptModel>> = _healthPrompts

    private val _timelineEvents = MutableLiveData<List<TimelineEventModel>>()
    val timelineEvents: LiveData<List<TimelineEventModel>> = _timelineEvents

    private val _onLoading = MutableLiveData<Unit>()
    val onLoading: LiveData<Unit> = _onLoading

    private val _onError = MutableLiveData<String?>()
    val onError: LiveData<String?> = _onError

    private val _onSuccess = MutableLiveData<Unit>()
    val onSuccess: LiveData<Unit> = _onSuccess

    fun retrieve() {
        _onLoading.postValue(Unit)
        viewModelScope.launch {
            try {
                onProfileSuccess(this, profilesRepository.retrieveProfiles())
            } catch (e: Throwable) {
                _onError.postValue(e.message)
            }
        }
    }

    private suspend fun onProfileSuccess(
        profileCallScope: CoroutineScope, profiles: List<Profile>
    ) {
        val profile = profiles.first()
        _profileHello.postValue(
            profileMapper.map(profile)
        )

        val healthPromptsRetrieve = profileCallScope.async { retrieveHealthPrompts(profile.id) }
        val timelineEventsRetrieve = profileCallScope.async { retrieveTimelineEvents(profile.id) }
        healthPromptsRetrieve.await()
        timelineEventsRetrieve.await()

        _onSuccess.postValue(Unit)
    }

    private suspend fun retrieveHealthPrompts(profileId: String) {
        val healthPrompts = healthPromptsRepository.retrieveHealthPrompts(profileId)
        _healthPrompts.postValue(
            healthPromptMapper.map(healthPrompts)
        )
    }

    private suspend fun retrieveTimelineEvents(profileId: String) {
        val timelineEvents = timelineEventsRepository.retrieveTimelineEvents(profileId)
        _timelineEvents.postValue(
            timelineEventMapper.map(timelineEvents)
        )
    }
}
