package com.ottonova.de.model

import android.text.format.DateUtils
import com.ottonova.de.data.TimelineEvent
import java.text.SimpleDateFormat
import java.util.*


class TimelineEventMapper {

    // 2020-03-15T08:00:00+00:00
    private val timestampFormat =
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())

    fun map(timelineEvents: List<TimelineEvent>): List<TimelineEventModel> =
        timelineEvents.map { timelineEvent ->
            val currentTimeMillis = System.currentTimeMillis()
            val parseTimestamp =
                timestampFormat.parse(timelineEvent.timestamp)?.time ?: currentTimeMillis
            val formatDate = DateUtils.getRelativeTimeSpanString(
                parseTimestamp, currentTimeMillis, DateUtils.MINUTE_IN_MILLIS
            ).toString()

            TimelineEventModel(
                timelineEvent.id,
                formatDate,
                timelineEvent.title,
                timelineEvent.description
            )
        }
}
