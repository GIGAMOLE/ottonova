package com.ottonova.de.model

import com.ottonova.de.data.HealthPrompt

class HealthPromptMapper {

    fun map(healthPrompts: List<HealthPrompt>): List<HealthPromptModel> =
        healthPrompts.map { healthPrompt ->
            val link = healthPrompt.metadata.link
            HealthPromptModel(
                link.title,
                link.url
            )
        }
}
