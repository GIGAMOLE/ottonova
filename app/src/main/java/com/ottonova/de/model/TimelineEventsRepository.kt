package com.ottonova.de.model

import com.ottonova.de.data.TimelineEvent
import com.ottonova.de.data.local.TimelineEventDao
import com.ottonova.de.data.remote.ApiClient

class TimelineEventsRepository(
    private val apiService: ApiClient.ApiService,
    private val timelineEventDao: TimelineEventDao,
    private val networkChecker: ApiClient.NetworkChecker
) {

    @Throws(Throwable::class)
    suspend fun retrieveTimelineEvents(profileId: String): List<TimelineEvent> {
        return if (networkChecker.isAvailable()) retrieveTimelineEventsRemote(profileId)
        else retrieveTimelineEventsLocal(profileId)
    }

    private suspend fun retrieveTimelineEventsRemote(profileId: String): List<TimelineEvent> {
        val timelineEvents = apiService.getTimelineEvents(profileId)
        timelineEventDao.insertAll(timelineEvents)

        return timelineEvents
    }

    private suspend fun retrieveTimelineEventsLocal(profileId: String): List<TimelineEvent> =
        timelineEventDao.getAll()
}
