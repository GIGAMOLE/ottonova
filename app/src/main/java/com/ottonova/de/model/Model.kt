package com.ottonova.de.model

import java.io.Serializable

data class HealthPromptModel(
    val title: String,
    val url: String
) : Serializable

data class TimelineEventModel(
    val id: String,
    val formatDate: String,
    val title: String,
    val description: String
) : Serializable
