package com.ottonova.de.model

import com.ottonova.de.data.Profile
import com.ottonova.de.data.local.ProfileDao
import com.ottonova.de.data.remote.ApiClient

class ProfilesRepository(
    private val apiService: ApiClient.ApiService,
    private val profileDao: ProfileDao,
    private val networkChecker: ApiClient.NetworkChecker
) {

    @Throws(Throwable::class)
    suspend fun retrieveProfiles(): List<Profile> {
        return if (networkChecker.isAvailable()) retrieveProfilesRemote()
        else retrieveProfilesLocal()
    }

    private suspend fun retrieveProfilesRemote(): List<Profile> {
        val profiles = apiService.getProfiles()
        profileDao.insertAll(profiles)

        return profiles
    }

    private suspend fun retrieveProfilesLocal(): List<Profile> = profileDao.getAll()
}
