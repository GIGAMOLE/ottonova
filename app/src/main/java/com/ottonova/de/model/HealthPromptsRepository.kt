package com.ottonova.de.model

import com.ottonova.de.data.HealthPrompt
import com.ottonova.de.data.local.HealthPromptDao
import com.ottonova.de.data.remote.ApiClient

class HealthPromptsRepository(
    private val apiService: ApiClient.ApiService,
    private val healthPromptDao: HealthPromptDao,
    private val networkChecker: ApiClient.NetworkChecker
) {

    @Throws(Throwable::class)
    suspend fun retrieveHealthPrompts(profileId: String): List<HealthPrompt> {
        return if (networkChecker.isAvailable()) retrieveHealthPromptsRemote(profileId)
        else retrieveHealthPromptsLocal(profileId)
    }

    private suspend fun retrieveHealthPromptsRemote(profileId: String): List<HealthPrompt> {
        val healthPrompts = apiService.getHealthPrompts(profileId)
        healthPromptDao.insertAll(healthPrompts)

        return healthPrompts
    }

    private suspend fun retrieveHealthPromptsLocal(profileId: String): List<HealthPrompt> =
        healthPromptDao.getAll()
}
