package com.ottonova.de.model

import android.content.Context
import com.ottonova.de.R
import com.ottonova.de.data.Profile

class ProfileMapper(private val context: Context) {

    private val helloFormat = context.getString(R.string.format_hello)

    fun map(profile: Profile): String = helloFormat.format(profile.displayName)
}
