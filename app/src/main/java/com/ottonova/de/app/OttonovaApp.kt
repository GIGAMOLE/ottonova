package com.ottonova.de.app

import android.app.Application
import coil.Coil
import coil.ImageLoader
import coil.util.CoilUtils
import com.ottonova.de.data.local.DatabaseClient
import com.ottonova.de.data.remote.ApiClient
import com.ottonova.de.data.remote.NetworkChangeReceiver
import com.ottonova.de.model.*
import com.ottonova.de.viewmodel.MainViewModel
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

@Suppress("unused")
class OttonovaApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@OttonovaApp)
            modules(
                listOf(
                    module,
                    ApiClient.module,
                    DatabaseClient.module
                )
            )
        }

        Coil.setDefaultImageLoader {
            ImageLoader(this) {
                crossfade(true)
                okHttpClient {
                    OkHttpClient.Builder()
                        .cache(CoilUtils.createDefaultCache(this@OttonovaApp))
                        .build()
                }
            }
        }
    }

    // App module
    private val module = module {
        viewModel { MainViewModel(get(), get(), get(), get(), get(), get()) }

        single { ProfileMapper(get()) }
        single { HealthPromptMapper() }
        single { TimelineEventMapper() }

        factory { NetworkChangeReceiver(get()) }
        factory { ProfilesRepository(get(), get(), get()) }
        factory { HealthPromptsRepository(get(), get(), get()) }
        factory { TimelineEventsRepository(get(), get(), get()) }
    }
}
