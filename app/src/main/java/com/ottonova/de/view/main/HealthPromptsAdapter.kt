package com.ottonova.de.view.main

import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.ottonova.de.R
import com.ottonova.de.model.HealthPromptModel

abstract class HealthPromptsAdapter : BaseQuickAdapter<HealthPromptModel, BaseViewHolder>(
    R.layout.item_health_prompt,
    arrayListOf()
), View.OnClickListener {

    override fun convert(holder: BaseViewHolder, item: HealthPromptModel) {
        holder.setText(R.id.itemHealthPromptTxtTitle, item.title)

        val itemView = holder.getView<View>(R.id.itemHealthPrompt)
        itemView.setOnClickListener(this)
        itemView.tag = item
    }

    override fun onClick(v: View?) = onItemClick(v?.tag as HealthPromptModel)

    abstract fun onItemClick(model: HealthPromptModel)
}
