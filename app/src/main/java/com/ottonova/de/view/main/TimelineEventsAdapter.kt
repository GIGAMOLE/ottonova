package com.ottonova.de.view.main

import android.view.View
import com.chad.library.adapter.base.BaseSectionQuickAdapter
import com.chad.library.adapter.base.entity.SectionEntity
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.ottonova.de.R
import com.ottonova.de.model.TimelineEventModel
import com.ottonova.de.view.main.TimelineEventsAdapter.TimelineEventSection

abstract class TimelineEventsAdapter :
    BaseSectionQuickAdapter<TimelineEventSection, BaseViewHolder>(
        R.layout.item_timeline_event_header,
        arrayListOf()
    ), View.OnClickListener {

    init {
        setNormalLayout(R.layout.item_timeline_event)
    }

    fun setData(newData: List<TimelineEventModel>) {
        data.clear()
        newData.groupBy { timelineEventModel -> timelineEventModel.formatDate }
            .forEach { (formatDate, timelineEventModels) ->
                data.add(TimelineEventSection(formatDate))
                timelineEventModels.forEach { model ->
                    data.add(TimelineEventSection(model))
                }
            }


        notifyDataSetChanged()
    }

    override fun convertHeader(helper: BaseViewHolder, item: TimelineEventSection) {
        helper.setText(R.id.itemTimelineEventHeaderTxt, item.formatData)
    }

    override fun convert(holder: BaseViewHolder, item: TimelineEventSection) {
        holder.setText(R.id.itemTimelineEventTxtTitle, item.model?.title)
        holder.setText(R.id.itemTimelineEventTxtSubtitle, item.model?.description)

        val itemView = holder.getView<View>(R.id.itemTimelineEvent)
        itemView.setOnClickListener(this)
        itemView.tag = item.model
    }

    override fun onClick(v: View?) = onItemClick(v?.tag as TimelineEventModel)

    abstract fun onItemClick(model: TimelineEventModel)

    inner class TimelineEventSection private constructor(
        override val isHeader: Boolean
    ) : SectionEntity {
        var model: TimelineEventModel? = null
        var formatData: String? = null

        constructor(model: TimelineEventModel) : this(false) {
            this.model = model
        }

        constructor(formatData: String) : this(true) {
            this.formatData = formatData
        }
    }
}
