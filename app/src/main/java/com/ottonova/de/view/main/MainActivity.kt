package com.ottonova.de.view.main

import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.ottonova.de.R
import com.ottonova.de.data.remote.NetworkChangeReceiver
import com.ottonova.de.model.HealthPromptModel
import com.ottonova.de.model.TimelineEventModel
import com.ottonova.de.view.details.TimelineEventDetailsActivity
import com.ottonova.de.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val mainViewModel: MainViewModel by viewModel()
    private val networkChangeReceiver: NetworkChangeReceiver by inject()

    private lateinit var healthPromptsAdapter: HealthPromptsAdapter
    private lateinit var timelineEventsAdapter: TimelineEventsAdapter
    private lateinit var offlineSnackbar: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModel()
        setupUI()

        retrieve()
    }

    override fun onStart() {
        registerNetworkChangeReceiver()
        super.onStart()
    }

    override fun onStop() {
        unregisterNetworkChangeReceiver()
        super.onStop()
    }

    private fun setupViewModel() {
        mainViewModel.profileHello.observe(this, Observer { textHello ->
            mainTxtHello.text = textHello
        })
        mainViewModel.onError.observe(this, Observer { message ->
            setState(State.ERROR)
            showMessageError(message)
        })
        mainViewModel.onLoading.observe(this, Observer {
            setState(State.LOADING)
        })
        mainViewModel.onSuccess.observe(this, Observer {
            setState(State.SUCCESS)
        })
        mainViewModel.healthPrompts.observe(this, Observer { healthPrompts ->
            healthPromptsAdapter.setNewInstance(healthPrompts.toMutableList())
        })
        mainViewModel.timelineEvents.observe(this, Observer { timelineEvents ->
            timelineEventsAdapter.setData(timelineEvents)
        })

        offlineSnackbar = Snackbar.make(
            mainCoordinator, R.string.error_no_network, Snackbar.LENGTH_INDEFINITE
        ).apply {
            setBackgroundTint(
                ContextCompat.getColor(this@MainActivity, R.color.offline)
            )
        }
        networkChangeReceiver.onNetworkChanged = callback@ ({ isAvailable ->
            if (!isAvailable && offlineSnackbar.isShown) return@callback

            if (isAvailable) {
                if (offlineSnackbar.isShown) retrieve()
                offlineSnackbar.dismiss()
            } else offlineSnackbar.show()
        })
    }

    private fun setupUI() {
        healthPromptsAdapter = object : HealthPromptsAdapter() {
            override fun onItemClick(model: HealthPromptModel) = openUrl(model.url)
        }
        timelineEventsAdapter = object : TimelineEventsAdapter() {
            override fun onItemClick(model: TimelineEventModel) {
                startActivity(
                    TimelineEventDetailsActivity.launchIntent(this@MainActivity, model)
                )
            }
        }

        val pageMargin = resources.getDimension(R.dimen.offset_regular) * 1.5F
        mainPagerHealthPrompts.setPageTransformer { page, position ->
            page.translationX = position * -pageMargin
        }
        mainPagerHealthPrompts.offscreenPageLimit = 4
        mainPagerHealthPrompts.adapter = healthPromptsAdapter

        mainListTimelineEvents.setHasFixedSize(true)
        mainListTimelineEvents.adapter = timelineEventsAdapter

        mainBtnReload.setOnClickListener { retrieve() }
    }

    private fun openUrl(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        try {
            startActivity(browserIntent)
        } catch (e: Exception) {
            showMessageError(null)
        }
    }

    // Show custom error message after initial response
    private fun showMessageError(message: String?) {
        val errorMessage = message?.let {
            resources.getString(R.string.error_message, message)
        } ?: resources.getString(R.string.error_message_default)

        Snackbar.make(mainCoordinator, errorMessage, Snackbar.LENGTH_LONG).apply {
            setBackgroundTint(
                ContextCompat.getColor(this@MainActivity, R.color.dark)
            )
        }.show()
    }

    private fun retrieve() = mainViewModel.retrieve()

    private fun setState(state: State) {
        val index = state.ordinal

        if (mainStates.displayedChild == index) return
        mainStates.displayedChild = index
    }

    // We can use registerNetworkCallback() from ConnectivityManager also
    @Suppress("DEPRECATION")
    private fun registerNetworkChangeReceiver() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) registerReceiver(
            networkChangeReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    private fun unregisterNetworkChangeReceiver() {
        try {
            unregisterReceiver(networkChangeReceiver)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
    }

    enum class State {
        LOADING, ERROR, SUCCESS
    }
}
