package com.ottonova.de.view.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ottonova.de.R
import com.ottonova.de.model.TimelineEventModel
import kotlinx.android.synthetic.main.activity_timeline_event_details.*

class TimelineEventDetailsActivity : AppCompatActivity(R.layout.activity_timeline_event_details) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val model = parseIntent(intent)
        if (model == null) {
            finish()
            return
        }

        setUI(model)
    }

    private fun setUI(model: TimelineEventModel) {
        timelineEventTxtDate.text = model.formatDate
        timelineEventTxtTitle.text = model.title
        timelineEventTxtSubtitle.text = model.description
    }

    companion object {
        private const val extraModel = "extraModel"

        fun launchIntent(context: Context, timelineEventModel: TimelineEventModel): Intent {
            val intent = Intent(context, TimelineEventDetailsActivity::class.java)
            intent.putExtra(extraModel, timelineEventModel)
            return intent
        }

        private fun parseIntent(intent: Intent): TimelineEventModel? =
            intent.getSerializableExtra(extraModel) as TimelineEventModel
    }
}
